#ifndef MONITORCONTROLUI_H
#define MONITORCONTROLUI_H

#include <QWidget>
#include "ui_MonitorControlUi.h"

namespace Ui {
class MonitorControlUi;
}

class MonitorControlUi : public QWidget
{
    Q_OBJECT

public:
    explicit MonitorControlUi(QStringList MonitorList ,QWidget *parent = nullptr);
    ~MonitorControlUi();

private:
    Ui::MonitorControlUi *ui;
    QStringList MonitorList;
};

#endif // MONITORCONTROLUI_H
