#include "MonitorControlUi.h"

MonitorControlUi::MonitorControlUi(QStringList MonitorList ,QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MonitorControlUi)
{
    ui->setupUi(this);

    ui->MonitorComboBox->clear();
    ui->MonitorComboBox->addItems(MonitorList);
}

MonitorControlUi::~MonitorControlUi()
{
    delete ui;
}
